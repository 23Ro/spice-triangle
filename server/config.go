package main

import (
	"os"
	"strconv"

	db "23Ro/spice-triangle/database"
)

type Config struct {
	Port        string
	Environment string
	DbConfig    db.DbConfig
}

func loadEnv() Config {
	port := os.Getenv("PORT")
	environment := os.Getenv("APP_ENVIRONMENT")
	db := new(db.DbConfig)
	db.DbName = os.Getenv("DB_NAME")
	db.Pasword = os.Getenv("DB_PWD")
	db.Port, _ = strconv.Atoi(os.Getenv("DB_PORT"))
	db.User = os.Getenv("DB_USER")
	db.Host = os.Getenv("DB_HOST")
	db.UseSSL, _ = strconv.ParseBool(os.Getenv("DB_SSL"))

	return Config{
		Port:        port,
		Environment: environment,
		DbConfig:    *db,
	}
}

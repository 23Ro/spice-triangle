package main

import (
	"net/http"

	"github.com/go-playground/validator"
	"github.com/joho/godotenv"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"

	"23Ro/spice-triangle/api/auth"
	db "23Ro/spice-triangle/database"
	"23Ro/spice-triangle/shopping-lists"
	"23Ro/spice-triangle/users"
)

var ENV = "development"

var L *log.Logger

type CustomValidator struct {
	validator *validator.Validate
}

func (cv *CustomValidator) Validate(i interface{}) error {
	if err := cv.validator.Struct(i); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	return nil
}

func main() {
	e := echo.New()
	// Init Logger
	L, ok := e.Logger.(*log.Logger)
	if !ok {
		L.SetHeader("${time_rfc3339} ${level}")
	}

	if ENV == "development" {
		e.Debug = true
		L.Print(ENV)
		L.Debug("Running in Debug Mode")
	}

	var config Config

	if config.Environment == "" {
		config.Environment = "development"
		err := godotenv.Load(".env.local")
		if err != nil {
			L.Error(err)
			panic(err)
		}
	}
	if config.Environment == "production" {
		err := godotenv.Load(".env")
		if err != nil {
			L.Error(err)
			panic(err)
		}
	}

	config = loadEnv()
	log.Print(config)
	db.InitDb(config.DbConfig)
	healthy := db.IsHealthyConnection()
	if healthy != true {
		L.Panic("DB not healthy")
	}
	db.RunDbMigration()

	e.HideBanner = true
	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Validator = &CustomValidator{validator: validator.New()}

	// Routes

	e.POST("/login", auth.Login)
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	e.GET("/login", func(c echo.Context) error {
		return c.String(http.StatusOK, "test")
	})
	gV0 := e.Group("/v0")
	{
		gV0.Use(echojwt.WithConfig(auth.AuthConfig))

		gUsers := gV0.Group("/users")
		gUsers.POST("/", users.CreateUser)
		gUsers.PUT("/", users.PutUser)
		gUsers.DELETE("/:id", users.DeleteUser)
		gLists := gV0.Group("/lists")
		gListsShopping := gLists.Group("/shopping")
		gListsShopping.POST("/", shoppinglists.CreateList)
		gListsShopping.POST("/:id/item", shoppinglists.CreateListItem)
		gLists.GET("/", func(c echo.Context) error { return c.String(200, "test") })
	}
	e.Logger.Fatal(e.Start(":1323"))
}

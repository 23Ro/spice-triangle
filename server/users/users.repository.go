package users

import (
	"context"
	"errors"
	"log"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5"

	db "23Ro/spice-triangle/database"
)

var (
	ErrUserNotFound          = errors.New("User not found")
	ErrNoUsersOnTenant       = errors.New("No users on tenant")
	ErrUnknown               = errors.New("Error unknown")
	ErrCreatingUser          = errors.New("User not created")
	ErrEmailAlreadyExists    = errors.New("User Email already exists")
	ErrUsernameAlreadyExists = errors.New("UserName already exists")
)

func save(user User) (User, error) {
	if err := db.DbPool.Ping(context.Background()); err == db.ErrPingFailed {
		log.Println(err)
		log.Panic("Fatal, could not establish database connection")
	}

	log.Println(user)

	insert := `
    INSERT into users(
      id,
      email,
      username,
      password,
      admin
    ) values (
      $1,
      $2,
      $3,
      $4,
      $5
    )`

	_, e := db.DbPool.Exec(context.Background(), insert,
		user.Id,
		user.Email,
		user.Username,
		user.Password,
		user.Admin,
	)

	if e != nil {
		switch e.Error() {
		case "ERROR: duplicate key value violates unique constraint \"users_email_key\" (SQLSTATE 23505)":
			return User{}, ErrEmailAlreadyExists

		case "ERROR: duplicate key value violates unique constraint \"users_username_key\" (SQLSTATE 23505)":
			return User{}, ErrUsernameAlreadyExists

		default:
			return User{}, ErrCreatingUser
		}
	}

	storedUser, e := getById(user.Id)
	if e != nil {
		log.Print("Fatal Error: Could not get stored User")
		log.Print(e.Error())
		panic(e)
	}
	return storedUser, nil
}

func getById(id uuid.UUID) (User, error) {
	if err := db.DbPool.Ping(context.Background()); err == db.ErrPingFailed {
		log.Println(err)
		log.Panic("Fatal, could not establish database connection")
	}

	query := `SELECT 
      id,
      username,
      email,
      admin,
      created_at,
      updated_at,
      deleted_at
    FROM users
    WHERE id = $1 AND deleted_at IS NULL;
    `
	user := new(User)
	err := db.DbPool.QueryRow(context.Background(), query, id).Scan(
		&user.Id,
		&user.Username,
		&user.Email,
		&user.Admin,
		&user.CreatedAt,
		&user.UpdateAt,
		&user.DeletedAt,
	)
	if err != nil {
		if err == pgx.ErrNoRows {
			return User{}, ErrUserNotFound
		} else if err != nil {
			log.Println(err)
			return User{}, ErrUnknown
		}
	}
	return *user, nil
}

func GetByEmail(email string) (User, error) {
	if err := db.DbPool.Ping(context.Background()); err == db.ErrPingFailed {
		log.Println(err)
		log.Panic("Fatal, could not establish database connection")
	}

	query := `SELECT 
      id,
      username,
      email,
      admin,
      password,
      created_at,
      updated_at,
      deleted_at
    FROM users
    WHERE email = $1 AND deleted_at IS NULL;
    `
	user := new(User)
	err := db.DbPool.QueryRow(context.Background(), query, email).Scan(
		&user.Id,
		&user.Username,
		&user.Email,
		&user.Admin,
		&user.Password,
		&user.CreatedAt,
		&user.UpdateAt,
		&user.DeletedAt,
	)
	if err != nil {
		if err == pgx.ErrNoRows {
			return User{}, ErrUserNotFound
		} else if err != nil {
			log.Println(err)
			return User{}, ErrUnknown
		}
	}
	return *user, nil
}

func get(limit int32, page int32) ([]User, error) {
	return []User{}, nil
}

func update(User) (User, error) {
	return User{}, nil
}

func softDelete(uuid.UUID) error {
	return nil
}

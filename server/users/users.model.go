package users

import (
	"database/sql"

	"github.com/google/uuid"
)

type User struct {
	Id        uuid.UUID    `db:"id" json:"id"`
	CreatedAt sql.NullTime `db:"created_at" json:"createdAt"`
	UpdateAt  sql.NullTime `db:"updated_at" json:"updatedAt"`
	DeletedAt sql.NullTime `db:"deleted_at" json:"deletedAt"`
	Email     string       `db:"email" json:"email"`
	Username  string       `db:"username" json:"username"`
	Password  string       `db:"password" json:"-"`
	Admin     bool         `json:"admin" db:"admin"`
}

type UsersCreateRequestDTO struct {
	Email    string `json:"email" validate:"required"`
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
	Admin    bool   `json:"admin" validate:"required"`
}

type UserResponseDTO struct {
	Id        uuid.UUID    `db:"id" json:"id"`
	CreatedAt sql.NullTime `db:"created_at" json:"created_at"`
	UpdatedAt sql.NullTime `db:"updated_at" json:"updated_at"`
	DeletedAt sql.NullTime `db:"deleted_at" json:"deleted_at"`
	Email     string       `db:"email" json:"email"`
	Username  string       `db:"username" json:"username"`
	Admin     bool         `json:"type" db:"type"`
}

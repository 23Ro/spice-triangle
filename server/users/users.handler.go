package users

import (
	"log"
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"

	"23Ro/spice-triangle/api/bcrypt"
)

type Path struct {
	Id uuid.UUID `query:"id"`
}

func CreateUser(c echo.Context) error {
	userDTO := new(UsersCreateRequestDTO)
	user := new(User)
	if err := c.Bind(userDTO); err != nil {
		return c.JSON(http.StatusBadRequest, "bad reqest")
	}
	if err := c.Validate(userDTO); err != nil {
		return err
	}
	user.Id = uuid.New()
	user.Admin = userDTO.Admin
	user.Email = userDTO.Email
	user.Username = userDTO.Username
	hashedPwd, err := bcrypt.HashPassword(userDTO.Password)
	if err != nil {
		c.Logger().Warn("Could not hash user password!")
		c.Logger().Error(err)
		return c.JSON(http.StatusInternalServerError, "Hashing user password failed!")
	}

	user.Password = hashedPwd

	createdUser, err := save(*user)
	if err != nil {
		switch err {
		case ErrEmailAlreadyExists:
			return c.JSON(http.StatusBadRequest, `{"error": "User Email already exists"}`)
		case ErrUsernameAlreadyExists:
			return c.JSON(http.StatusBadRequest, `{"error": "Username already exists"}`)
		default:
			return c.JSON(http.StatusInternalServerError, nil)
		}
	}
	return c.JSON(http.StatusCreated, createdUser)
}

func PutUser(c echo.Context) (err error) {
	path := new(Path)
	user := &User{}

	if err := (&echo.DefaultBinder{}).BindPathParams(c, path); err != nil {
		return err
	}
	if err := (&echo.DefaultBinder{}).BindBody(c, user); err != nil {
		return err
	}
	user.Id = path.Id
	if updatedUser, err := update(*user); err != nil {
		log.Println(err)
		return c.JSON(http.StatusInternalServerError, nil)
	} else {
		return c.JSON(http.StatusOK, updatedUser)
	}
}

func DeleteUser(c echo.Context) (err error) {
	path := new(Path)

	if err := (&echo.DefaultBinder{}).BindPathParams(c, path); err != nil {
		return err
	}
	if err := softDelete(path.Id); err != nil {
		return c.JSON(http.StatusInternalServerError, nil)
	} else {
		return c.JSON(http.StatusOK, nil)
	}
}

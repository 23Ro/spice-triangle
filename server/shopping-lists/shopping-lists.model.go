package shoppinglists

import (
	"database/sql"

	"github.com/google/uuid"
)

type ShoppingList struct {
	Id          uuid.UUID      `db:"id" json:"id"`
	CreatedAt   sql.NullTime   `db:"created_at" json:"createdAt"`
	UpdateAt    sql.NullTime   `db:"updated_at" json:"updatedAt"`
	DeletedAt   sql.NullTime   `db:"deleted_at" json:"deletedAt"`
	Name        string         `db:"name" json:"name"`
	Description sql.NullString `db:"description" json:"description"`
}

type ShoppingListItem struct {
	Id        uuid.UUID    `db:"id" json:"id"`
	CreatedAt sql.NullTime `db:"created_at" json:"createdAt"`
	UpdateAt  sql.NullTime `db:"updated_at" json:"updatedAt"`
	DeletedAt sql.NullTime `db:"deletedAt" json:"deletedAt"`
	Name      string       `db:"name" json:"name"`
	AddedBy   string       `db:"added_by" json:"addedBy"`
	BoughtAt  sql.NullTime `db:"bought_at" json:"boughtAt"`
	BoughtBy  string       `db:"bought_by" json:"boughtBy"`
}

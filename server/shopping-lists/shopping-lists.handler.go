package shoppinglists

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

type Path struct {
	Id string `query:"id"`
}

func CreateList(c echo.Context) error {
	listId := uuid.New()
	list := &ShoppingList{
		Id: listId,
	}

	if err := c.Bind(list); err != nil {
		return err
	}
	return c.JSON(http.StatusCreated, list)
}

func CreateListItem(c echo.Context) (err error) {
	path := new(Path)
	listItem := new(ShoppingListItem)
	if err := (&echo.DefaultBinder{}).BindPathParams(c, path); err != nil {
		return err
	}
	if err := (&echo.DefaultBinder{}).BindBody(c, listItem); err != nil {
		return err
	}

	listItem.Id = uuid.New()
	// save(listId, listItem)

	return c.JSON(http.StatusOK, listItem)
}

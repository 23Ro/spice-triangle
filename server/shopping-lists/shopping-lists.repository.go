package shoppinglists

func save(ShoppingList) (ShoppingList, error) {
	return ShoppingList{}, nil
}

func get(id string) (ShoppingList, error) {
	return ShoppingList{}, nil
}

func getAll(limit int32, page int32) ([]ShoppingList, error) {
	return []ShoppingList{}, nil
}

func createItem(ShoppingListItem) (ShoppingListItem, error) {
	return ShoppingListItem{}, nil
}

CREATE TABLE IF NOT EXISTS shopping_lists(
  id uuid PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now(),
  deleted_at timestamptz,
  name varchar NOT NULL,
  description varchar 
);

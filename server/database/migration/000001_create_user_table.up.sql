CREATE TABLE IF NOT EXISTS users(
  id uuid PRIMARY KEY,
  email varchar NOT NULL UNIQUE,
  username varchar NOT NULL UNIQUE,
  password varchar NOT NULL,
  admin boolean NOT NULL DEFAULT false,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now(),
  deleted_at timestamptz
);

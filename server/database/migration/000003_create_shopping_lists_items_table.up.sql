CREATE TABLE IF NOT EXISTS shopping_list_items(
  id uuid PRIMARY KEY,
  created_at timestamptz NOT NULL DEFAULT now(),
  updated_at timestamptz NOT NULL DEFAULT now(),
  deleted_at timestamptz,
  name varchar NOT NULL,
  added_by uuid NOT NULL REFERENCES users(id),
  bought_at timestamptz,
  bought_by uuid REFERENCES users(id)
);

package database

import (
	"context"
	"errors"
	"log"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jackc/pgx/v5/pgxpool"
)

var ErrPingFailed = errors.New("Pinging Database failed")

type DbConfig struct {
	Port    int
	User    string
	Host    string
	Pasword string
	UseSSL  bool
	DbName  string
}

var (
	DbPool             *pgxpool.Pool
	DbConnectionString string
	ctx                = context.Background()
)

func InitDb(conf DbConfig) {
	var err error
	DbConnectionString = "postgresql://" + conf.User + ":" + conf.Pasword + "@" + conf.Host + "/" + conf.DbName
	if !conf.UseSSL {
		DbConnectionString = DbConnectionString + "?sslmode=disable"
	}
	log.Println(DbConnectionString)
	DbPool, err = pgxpool.New(context.Background(), DbConnectionString)
	if err != nil {
		log.Panicln(err)
	}

	if err = DbPool.Ping(context.Background()); err != nil {
		log.Panicln(err)
	}

	log.Println("DB connection established")
}

func IsHealthyConnection() bool {
	err := DbPool.Ping(context.Background())
	if err != nil {
		log.Println(err)
		return false
	}
	return true
}

func RunDbMigration() {
	log.Println("Running migrations")
	migration, err := migrate.New("file://database/migration", DbConnectionString)
	if err != nil {
		log.Println("Cannot create new migration instance!")
		log.Panicln(err)
	}
	if err := migration.Up(); err != nil && err != migrate.ErrNoChange {
		log.Println("Failed to run migration up!")
		log.Panicln(err)
	}
	log.Println("Migration successfull!")
}

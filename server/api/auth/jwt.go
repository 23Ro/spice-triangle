package auth

import (
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v5"
	echojwt "github.com/labstack/echo-jwt/v4"
	"github.com/labstack/echo/v4"

	"23Ro/spice-triangle/api/bcrypt"
	"23Ro/spice-triangle/users"
)

type jwtCustomClaims struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Admin    bool   `json:"admin"`
	jwt.RegisteredClaims
}

var AuthConfig = echojwt.Config{
	NewClaimsFunc: func(c echo.Context) jwt.Claims {
		return new(jwtCustomClaims)
	},
	SigningKey: []byte("secret"),
}

func Login(c echo.Context) error {
	email := c.FormValue("email")
	password := c.FormValue("password")
	c.Echo().Logger.Print(email)
	c.Echo().Logger.Print(password)
	user, err := users.GetByEmail(email)
	if err != nil {
		switch err {
		case users.ErrUserNotFound:
			return echo.ErrNotFound
		default:
			return echo.ErrUnauthorized
		}
	}

	if !bcrypt.ComparePassword(password, user.Password) {
		return echo.ErrUnauthorized
	}
	claims := &jwtCustomClaims{
		user.Email,
		user.Username,
		user.Admin,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * 72)),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte("secret"))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{
		"token": t,
	})
}

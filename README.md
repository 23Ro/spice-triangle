# Spice-Triangle

> A small and straight forward app for shared living/apartments/families/friends to manage shopping lists, expense splitting, tasks and other common things of life. The name comes from the spice triangle in Star Wars, which used to consist out of three hyperspace routes for spice trade. Spice being the plague of the galaxy, an addictive drug. The app has gotten the title because we use a metric fton of cinnamon in our flat.

## Getting Started

A `docker-compose.yml` file in the root directory spins up the PG instance and adminer.
For local development, a .env.local is provided setting the necessary environment information.

## Deployments

TBD

## Server

A simple Golang REST API that allows to manage:

- Shopping lists
- Users

## Client

A cross platform fyne client for the spice-triangle server
